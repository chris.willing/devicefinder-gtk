/*
* Compile me with:
*   gcc -o devicefinder-gtk devicefinder-gtk.c $(pkg-config --cflags --libs gtk+-2.0 gmodule-2.0)
*/

#include "devicefinder-gtk.h"

Config	configuration;

int
main( int    argc,
char **argv )
{
		GtkBuilder *builder;
		GtkWidget  *window;

		df_setup(argc, argv);
		//printf("[mqtt_addr = %s, mqtt_port = %d]\n", configuration.mqtt_addr, configuration.mqtt_port);

		gtk_init( &argc, &argv );

		df_mqtt_connect();

		builder = gtk_builder_new_from_resource("/com/darlo/devicefinder-gtk/devicefinder-gtk.glade");

		window = GTK_WIDGET( gtk_builder_get_object( builder, "window_main" ) );
		g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

		configuration.device_list_box = GTK_BOX( gtk_builder_get_object( builder, "device_list_box" ) );
		configuration.device_info_display_label = GTK_LABEL( gtk_builder_get_object( builder, "device_info_display_label" ) );
		configuration.show_data_label = GTK_LABEL( gtk_builder_get_object( builder, "label_data" ) );
		configuration.label_connection_status = GTK_LABEL( gtk_builder_get_object( builder, "label_connection_status" ) );
		configuration.indicator_connection_status = GTK_IMAGE( gtk_builder_get_object( builder, "indicator_connection_status" ) );

		gtk_builder_connect_signals(builder, NULL);

		configuration.context = g_main_context_default();


		/* Destroy builder, since we don't need it anymore */
		g_object_unref( G_OBJECT( builder ) );

		/* CSS */
		GtkCssProvider *provider;
		provider = gtk_css_provider_new ();
		gtk_css_provider_load_from_resource(provider, "/com/darlo/devicefinder-gtk/devicefinder-gtk.css");
		gtk_style_context_add_provider_for_screen(gdk_screen_get_default(), GTK_STYLE_PROVIDER(provider),GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
		g_object_unref (provider);

		/* Window Icon */
		GError *err = NULL;
		GdkPixbuf *pixbuf = gdk_pixbuf_new_from_resource("/com/darlo/devicefinder-gtk/XkeysIcon.png", &err);
		if (!pixbuf) {
			printf("error loading icon %s", err->message);
			g_error_free(err);
			return -1;
		}
		gtk_window_set_icon(GTK_WINDOW(window), pixbuf);

		/* Heartbeat check */
		configuration.last_seen = g_date_time_new_now_local();
		guint timer_id = g_timeout_add_seconds(5, check_heartbeat, NULL);
		if (timer_id < 1) {
			printf("Couldn't set heartbeat check timer.\n");
			return -1;
		}


		/* Show window. All other widgets are automatically shown by GtkBuilder */
		gtk_widget_show( window );
		gtk_main();

		return 0;
}

static gboolean
check_heartbeat() {
	GDateTime *now = g_date_time_new_now_local();
	gint64 now_last_diff = g_date_time_difference(now, configuration.last_seen);
	if (now_last_diff > 4000000) {
		//printf("not seen in time\n");
		gtk_image_set_from_icon_name(configuration.indicator_connection_status, "gtk-no", GTK_ICON_SIZE_BUTTON);
		gtk_label_set_text(GTK_LABEL(configuration.label_connection_status), "NOT connected");
	}

	return TRUE;
}

/* Find & update device_info_display_label for active device */
static void
which_checked(GtkWidget* widget, gpointer data) {
	JsonNode *node;

	if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget)) > 0 ) {
		node = g_object_get_data(G_OBJECT(widget), "radio_data_object");
	} else {
		return;
	}

	/* Pretty print */
	JsonGenerator *g = json_generator_new();
	json_generator_set_root(g, node);
	json_generator_set_pretty(g, TRUE);
	char *ppstr = json_generator_to_data(g, NULL);
	//printf("ppstr:\n%s\n", ppstr);
	gtk_label_set_text(GTK_LABEL(configuration.device_info_display_label), ppstr);
}
static void
delete_nodelist_element (gpointer data, gpointer user_data) {
	configuration.nodelist = g_list_remove(configuration.nodelist, data);
}
static void
remove_list_box_child(GtkWidget* widget, gpointer data) {
	gtk_container_remove(GTK_CONTAINER(configuration.device_list_box), widget);
}
/* Process data node for a device listing */
static void
forEachJsonMember(JsonObject *Object, const gchar* member_name, JsonNode *member_node, gpointer user_data) {
	//printf("forEachJsonElement(): %s\n", member_name);

	/* Add the node to our collection */
	configuration.nodelist = g_list_append(configuration.nodelist, member_node);
	add_button_for_device(member_name, member_node);
}
int
message_received(gpointer *msg_data) {
	//printf("msg_data:\n%s\n", (char*)msg_data);
	//printf("Length = %zu\n", strlen((char*)msg_data));

	JsonParser *parser = json_parser_new ();
	GError* err = NULL;
	if (!json_parser_load_from_data (parser, (const gchar*)msg_data, strlen((char*)msg_data), &err)) {
		printf("error in parsing json data %s", err->message);
		g_error_free(err);
		g_object_unref(parser);
		return -1;
	}

	JsonNode *root = json_parser_get_root (parser);
	JsonObject *jobj = json_node_get_object(root);

	if (json_object_has_member(jobj, "request")) {
		if (strcmp(json_object_get_string_member(jobj, "request"), "result_deviceList") == 0) {
			//printf("request is result_deviceList\n");
			/* Clear any existing listing */
			g_list_foreach(configuration.nodelist, delete_nodelist_element, NULL);
			g_list_free(g_steal_pointer(&configuration.nodelist));

			gtk_container_foreach(GTK_CONTAINER(configuration.device_list_box), remove_list_box_child, NULL);

			if (json_object_has_member(jobj, "data")) {
				JsonObject *data_obj = json_object_get_object_member(jobj, "data");
				json_object_foreach_member(data_obj, forEachJsonMember, NULL);
			}

			/* Since we've repopulated the list, ensure that correct device info is displayed */
			gtk_container_foreach(GTK_CONTAINER(configuration.device_list_box), which_checked, NULL);
			if (!(gtk_container_get_children(GTK_CONTAINER(configuration.device_list_box)))) {
				gtk_label_set_text(GTK_LABEL(configuration.device_info_display_label), "");
				gtk_label_set_text(GTK_LABEL(configuration.show_data_label), "");
			}

		} else if (strcmp(json_object_get_string_member(jobj, "request"), "device_event") == 0) {
			//printf("msg_data:\n%s\n", (char*)msg_data);
			if (json_object_has_member(jobj, "data")) {
				/* Format the data for display */
				JsonNode *data_node = json_object_dup_member(jobj, "data");
				JsonGenerator *g = json_generator_new();
				json_generator_set_root(g, data_node);
				//json_generator_set_pretty(g, TRUE);
				char *ppstr = json_generator_to_data(g, NULL);

				/* Update the data stream display */
				char *text = strdup(gtk_label_get_text(GTK_LABEL(configuration.show_data_label)));
				char *newtext = g_strdup_printf("%s%s\n", text, ppstr);
				gtk_label_set_text(GTK_LABEL(configuration.show_data_label), newtext);

				/* Scroll to end */
				GtkWidget *viewport;
				if ((viewport=gtk_widget_get_parent(GTK_WIDGET(configuration.show_data_label)))) {
					GtkAdjustment *adjustment = gtk_scrollable_get_vadjustment(GTK_SCROLLABLE(viewport));
					gtk_adjustment_set_value(adjustment, gtk_adjustment_get_upper(adjustment));
				}
			}
		} else if (strcmp(json_object_get_string_member(jobj, "request"), "heartbeat") == 0) {
			gtk_image_set_from_icon_name(configuration.indicator_connection_status, "gtk-yes", GTK_ICON_SIZE_BUTTON);
			gtk_label_set_text(GTK_LABEL(configuration.label_connection_status), "Connected OK");

			//if (configuration.last_seen) free(configuration.last_seen);
			configuration.last_seen = g_date_time_new_now_local();
		}
	}



	free(msg_data);
	return G_SOURCE_REMOVE;
}


/* Refresh device list */
void
button_find_clicked_cb() {
	publish_devicelist_request(configuration.mosq);
}

/* Refresh device info */
void
button_info_clicked_cb() {
	gtk_container_foreach(GTK_CONTAINER(configuration.device_list_box), which_checked, NULL);
	if (!(gtk_container_get_children(GTK_CONTAINER(configuration.device_list_box)))) {
		gtk_label_set_text(GTK_LABEL(configuration.device_info_display_label), "");
	}
}

/* Clear device data */
void
button_data_clicked_cb() {
	/* Refresh
	gtk_container_foreach(GTK_CONTAINER(configuration.device_list_box), which_checked, NULL);
	if (!(gtk_container_get_children(GTK_CONTAINER(configuration.device_list_box)))) {
		gtk_label_set_text(GTK_LABEL(configuration.show_data_label), "");
	}
	*/

	/* Clear */
	gtk_label_set_text(GTK_LABEL(configuration.show_data_label), "");
}

void
on_device_button_click(GtkWidget *widget, gpointer data) {
	g_print("click\n");

	//printf("Row #%i\n", gtk_list_box_row_get_index(GTK_LIST_BOX_ROW(data)));

	JsonObject *row_data_object = g_object_get_data(G_OBJECT(data), "row_data_object");
	if (row_data_object == NULL ) {
		printf("no data\n");
	}
}

static void
output_state (GtkToggleButton *source, gpointer user_data) {
	JsonNode *node;

	if (gtk_toggle_button_get_active(source) > 0 ) {
		node = g_object_get_data(G_OBJECT(source), "radio_data_object");
	} else {
		return;
	}

	JsonGenerator *g = json_generator_new();
	json_generator_set_root(g, node);
	json_generator_set_pretty(g, TRUE);
	char *ppstr = json_generator_to_data(g, NULL);
	//printf("ppstr:\n%s\n", ppstr);
	gtk_label_set_text(GTK_LABEL(configuration.device_info_display_label), ppstr);
}
static void
count_children(GtkWidget* widget, gpointer data) {
	*(int*)data += 1;
}
void
add_button_for_device(const gchar* member_name, JsonNode *member_node) {
	JsonObject *mobj = json_node_get_object(member_node);
	GtkWidget *radio_button;

	/* Is this the first? */
	int child_count = 0;
	gtk_container_foreach(GTK_CONTAINER(configuration.device_list_box), count_children, &child_count);

	char button_label_text[48];
	sprintf(button_label_text, "%s\t%s", member_name, json_object_get_string_member(mobj, "name"));
	GtkWidget	*button_label = gtk_label_new(button_label_text);
	gtk_widget_set_halign(GTK_WIDGET(button_label), GTK_ALIGN_START);

	if (child_count < 1) {
		configuration.device_list_radio_button = gtk_radio_button_new(NULL);
		gtk_container_add(GTK_CONTAINER(configuration.device_list_radio_button), button_label);
		gtk_toggle_button_set_mode(GTK_TOGGLE_BUTTON(configuration.device_list_radio_button), FALSE);

		g_object_set_data(G_OBJECT(configuration.device_list_radio_button), "radio_data_object", member_node);

		g_signal_connect (GTK_TOGGLE_BUTTON(configuration.device_list_radio_button), "toggled", G_CALLBACK(output_state), NULL);

		gtk_box_pack_start (GTK_BOX(configuration.device_list_box), configuration.device_list_radio_button, FALSE, FALSE, 1);
	} else {
		radio_button = gtk_radio_button_new_from_widget (GTK_RADIO_BUTTON(configuration.device_list_radio_button));
		gtk_container_add(GTK_CONTAINER(radio_button), button_label);
		gtk_toggle_button_set_mode(GTK_TOGGLE_BUTTON(radio_button), FALSE);

		g_object_set_data(G_OBJECT(radio_button), "radio_data_object", member_node);

		g_signal_connect (GTK_TOGGLE_BUTTON(radio_button), "toggled", G_CALLBACK(output_state), NULL);

		gtk_box_pack_start (GTK_BOX(configuration.device_list_box), radio_button, FALSE, FALSE, 1);
	}

	gtk_widget_show_all(GTK_WIDGET(configuration.device_list_box));
}

