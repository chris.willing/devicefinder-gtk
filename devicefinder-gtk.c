#include "devicefinder-gtk.h"

void on_mosq_connect(struct mosquitto*,void*,int);
void on_mosq_publish(struct mosquitto*,void*,int);
void on_mosq_subscribe(struct mosquitto*,void*,int,int,const int*);
void on_mosq_message(struct mosquitto*,void*,const struct mosquitto_message*);

void
df_setup(int argc, char **argv) {

	FILE	*fp;
	char	*mqtt_addr = (char*)"localhost";
	int		mqtt_port = 1883;
	char	*line = NULL;
	size_t len = 0;
	ssize_t nread;

	/* First ensure we have a config directory
	*/
	gchar *app_configdir = g_build_path("/", g_get_user_config_dir(), "devicefinder", (gchar*)0);
	if ((g_mkdir_with_parents(app_configdir, 0777) < 0)) {
		printf("Couldn't create directory: %s\n", app_configdir);
		exit(5);
	}

	/* Now the config file
	*/
	gchar *app_configfile = g_build_path("/", app_configdir, "devicefinder.conf", (gchar*)0);
	fp = fopen(app_configfile,"r+");
	if (!fp) {
		/*	No config file => 1st use of this application.
		*	Create a new config file based on either:
		*	1. command line args., or
		*	2. use a default
		*/
		if (argc == 1) {
			/*	No args provided so use default settings
			*/
			printf("Creating a new %s\n", app_configfile);
			mqtt_addr = (char*)"localhost";
			mqtt_port = 1883;
			fp = fopen(app_configfile,"w+");
			fprintf(fp, "MQTT_ADDR=%s\n", mqtt_addr);
			fprintf(fp, "MQTT_PORT=%d\n", mqtt_port);
			fclose(fp);
		} else if (argc == 3 ) {
			/*	Correct number of args */
			mqtt_addr = argv[1];
			mqtt_port = atoi(argv[2]);
			fp = fopen(app_configfile,"w+");
			fprintf(fp, "MQTT_ADDR=%s\n", mqtt_addr);
			fprintf(fp, "MQTT_PORT=%d\n", mqtt_port);
			fclose(fp);
		} else {
			fprintf(stderr, "Need MQTT server address and port number\n");
			exit(1);
		}
			
	}
	/*	By now we have a usable config file.
	*	However ignore the config file if given usable command line args.
	*/
	if (argc == 3) {
		/* Use command line args */
		mqtt_addr = argv[1];
		mqtt_port = atoi(argv[2]);

		/*	Overwrite the config file entries to match command line.
		*	We can just write a new file since
		*	MQTT_ADDR & MQTT_PORT are the only entries at the moment.
		*/
		fp = fopen(app_configfile,"w+");
		fprintf(fp, "MQTT_ADDR=%s\n", mqtt_addr);
		fprintf(fp, "MQTT_PORT=%d\n", mqtt_port);
		fclose(fp);
	} else {
		/* Use configfile settings */
		fp = fopen(app_configfile,"r");
		if (!fp ) {
			perror("fopen");
			exit(EXIT_FAILURE);
		}
		while ((nread = getline(&line, &len, fp)) != -1) {
			if (strncmp(line, "MQTT_ADDR=", strlen("MQTT_ADDR=")) == 0) {
				char *p = strrchr(line, '=');
				mqtt_addr = strdup(++p);
				mqtt_addr[(int)strlen(mqtt_addr) - 1] = 0;
			} else if (strncmp(line, "MQTT_PORT=", strlen("MQTT_PORT=")) == 0) {
				char *p = strrchr(line, '=');
				char *mqtt_port_p = strdup(++p);
				mqtt_port_p[(int)strlen(mqtt_port_p) - 1] = 0;
				mqtt_port = atoi(mqtt_port_p);
			}
		}
		if (line) free(line);
		fclose(fp);
		//printf("Using config file entries %s:%d\n", mqtt_addr, mqtt_port);
	}
	configuration.mqtt_addr = (char*)mqtt_addr;
	configuration.mqtt_port = mqtt_port;

	configuration.nodelist = NULL;
}

void
df_mqtt_connect() {
	struct mosquitto *mosq;
	int rc;

	mosquitto_lib_init();
	mosq = mosquitto_new(NULL, true, NULL);
	if (mosq == NULL ) {
		fprintf(stderr, "Error: Out of memory.\n");
		exit(2);
	}
	configuration.mosq = mosq;

	mosquitto_connect_callback_set(mosq, on_mosq_connect);	
	mosquitto_publish_callback_set(mosq, on_mosq_publish);
	mosquitto_subscribe_callback_set(mosq, on_mosq_subscribe);
	mosquitto_message_callback_set(mosq, on_mosq_message);

	rc = mosquitto_connect(mosq, configuration.mqtt_addr, configuration.mqtt_port, 60);
	if (rc != MOSQ_ERR_SUCCESS) {
		mosquitto_destroy(mosq);
		fprintf(stderr, "Error: %s\n", mosquitto_strerror(rc));
		exit(4);
	}

	publish_devicelist_request(mosq);

	mosquitto_loop_start(mosq);

}

void
on_mosq_connect(struct mosquitto *mosq, void *obj, int reason_code) {
	int rc;

	//printf("on_mosq_connect: %s\n", mosquitto_connack_string(reason_code));
	if (reason_code != 0) {
		mosquitto_disconnect(mosq);
	}

	rc = mosquitto_subscribe(mosq, NULL, "/xkeys/server/#", 1);
	if(rc != MOSQ_ERR_SUCCESS){
		fprintf(stderr, "Error subscribing: %s\n", mosquitto_strerror(rc));
		/* We might as well disconnect if we were unable to subscribe */
		mosquitto_disconnect(mosq);
		exit(3);
	}
}

void
on_mosq_publish(struct mosquitto *mosq, void *obj, int mid) {
	//printf("Message with mid %d has been published.\n", mid);
	return;
}

void
on_mosq_subscribe(struct mosquitto *mosq, void *obj, int mid, int qos_count, const int *granted_qos) {
	int i;
	bool have_subscription = false;

	/* In this example we only subscribe to a single topic at once, but a
	* SUBSCRIBE can contain many topics at once, so this is one way to check
	* them all.
	*/
	for(i=0; i<qos_count; i++){
		//printf("on_subscribe: %d:granted qos = %d\n", i, granted_qos[i]);
		if(granted_qos[i] <= 2){
			have_subscription = true;
		}
	}
	if(have_subscription == false){
		/* The broker rejected all of our subscriptions, we know we only sent
		* the one SUBSCRIBE, so there is no point remaining connected.
		*/
		fprintf(stderr, "Error: All subscriptions rejected.\n");
		mosquitto_disconnect(mosq);
		exit(4);
	}
}

void
on_mosq_message(struct mosquitto *mosq, void *obj, const struct mosquitto_message *msg) {
	GSource *source;
	char *msg_data = strdup(msg->payload);

	source = g_idle_source_new();
	g_source_set_callback(source, G_SOURCE_FUNC(message_received), (gpointer*)msg_data, NULL);
	g_source_attach(source, configuration.context);
	g_source_unref(source);
}

void
publish_devicelist_request(struct mosquitto *mosq) {
	char payload[30];
	int rc;

	/*	The request is for a list of connected devices */
	snprintf(payload, sizeof(payload), "%s", "{ \"request\": \"deviceList\" }");

	/* Publish the message
	* mosq - our client instance
	* *mid = NULL - we don't want to know what the message id for this message is
	* topic = "example/temperature" - the topic on which this message will be published
	* payloadlen = strlen(payload) - the length of our payload in bytes
	* payload - the actual payload
	* qos = 2 - publish with QoS 2 for this example
	* retain = false - do not use the retained message feature for this message
	*/
	rc = mosquitto_publish(mosq, NULL, "/xkeys/node", strlen(payload), payload, 2, false);
	if(rc != MOSQ_ERR_SUCCESS){
		fprintf(stderr, "Error publishing: %s\n", mosquitto_strerror(rc));
	}
}


