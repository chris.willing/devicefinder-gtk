#include <json-glib/json-glib.h>
#include <mosquitto.h>
#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>


typedef struct Config {
	char	*mqtt_addr;
	int		mqtt_port;
	struct mosquitto *mosq;
	GMainContext *context;
	GtkLabel	*device_info_display_label;
	GtkLabel	*show_data_label;
	GtkBox	*device_list_box;
	GtkWidget *device_list_radio_button;
	GList	*nodelist;
	GtkLabel *label_connection_status;
	GtkImage *indicator_connection_status;
	GDateTime *last_seen;
} Config;
extern Config	configuration;

void df_setup(int, char**);
void df_mqtt_connect();
int message_received(gpointer*);
void add_button_for_device(const gchar*, JsonNode*);
void publish_devicelist_request(struct mosquitto*);
static gboolean check_heartbeat();


