project(devicefinder C CXX)

set(PROJECT_TARGET devicefinder)

# Set the minimum version of cmake required to build this project
cmake_minimum_required(VERSION 3.10)


find_program(GLIB_COMPILE_RESOURCES NAMES glib-compile-resources REQUIRED)
set(GRESOURCE_C   devicefinder-gtk-resources.c)
set(GRESOURCE_XML devicefinder-gtk.gresource.xml)
add_custom_command(
	OUTPUT ${GRESOURCE_C}
	WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
	COMMAND ${GLIB_COMPILE_RESOURCES}
	ARGS
		--sourcedir=. --generate-source --target=${CMAKE_CURRENT_BINARY_DIR}/${GRESOURCE_C}
		${GRESOURCE_XML}
	VERBATIM
	MAIN_DEPENDENCY ${GRESOURCE_XML}
#	DEPENDS
#		devicefinder-gtk.glade
)
add_custom_target(
    devicefinder-gtk.resources.o
	DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/${GRESOURCE_C}
)

# Use the package PkgConfig to detect GTK+ headers/library files
find_package(PkgConfig REQUIRED)
pkg_check_modules(GTK REQUIRED gtk+-3.0)
pkg_check_modules(JSONGLIB REQUIRED json-glib-1.0)
pkg_check_modules(MOSQUITTO REQUIRED IMPORTED_TARGET libmosquitto)

# Create the executable
add_executable(${PROJECT_TARGET} main.c devicefinder-gtk.c devicefinder-gtk-resources.c ${CMAKE_CURRENT_BINARY_DIR}/${GRESOURCE_C})
set_source_files_properties(
	${CMAKE_CURRENT_BINARY_DIR}/${GRESOURCE_C}
	PROPERTIES GENERATED TRUE
)
add_dependencies(${PROJECT_NAME} devicefinder-gtk.resources.o)
target_link_libraries(${PROJECT_TARGET} PRIVATE ${GTK_LIBRARIES} ${JSONGLIB_LIBRARIES} ${MOSQUITTO_LIBRARIES} -rdynamic)

# Setup CMake to use GTK+, tell the compiler where to look for headers
# and to the linker where to look for libraries
target_include_directories(${PROJECT_TARGET} PRIVATE ${GTK_INCLUDE_DIRS} ${JSONGLIB_INCLUDE_DIRS} ${MOSQUITTO_INCLUDE_DIRS})
target_link_directories(${PROJECT_TARGET} PRIVATE ${GTK_LIBRARY_DIRS})
target_compile_options(${PROJECT_TARGET} PRIVATE ${GTK_CFLAGS_OTHER})

if(${CMAKE_SYSTEM_NAME} MATCHES "Linux")
	install(TARGETS ${PROJECT_TARGET} RUNTIME)
	install(FILES ${PROJECT_TARGET}.desktop DESTINATION share/applications )
	install(FILES assets/pielogox200.png  DESTINATION share/pixmaps RENAME ${PROJECT_TARGET}.png )
	if (${CMAKE_MAJOR_VERSION}.${CMAKE_MINOR_VERSION} GREATER 3.18)
		install(SCRIPT "${CMAKE_SOURCE_DIR}/PostInstall.cmake")
	endif (${CMAKE_MAJOR_VERSION}.${CMAKE_MINOR_VERSION} GREATER 3.18)
endif (${CMAKE_SYSTEM_NAME} MATCHES "Linux")

