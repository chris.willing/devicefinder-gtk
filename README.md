# devicefinder-gtk

This _devicefinder-gtk_ project is designed to demonstrate communication, using the MQTT protocol to transfer JSON encoded messages, with an [_xkeys-server_](https://gitlab.com/chris.willing/xkeys-server).

The [_mosquitto_ library](https://mosquitto.org) is used to implement the MQTT protocol and [_json-glib_](https://gitlab.gnome.org/GNOME/json-glib) for the JSON encoding & decoding. The [_GTK_](https://gtk.org/) toolkit is used for the graphical user interface.

In this project, we provide the _devicefinder_ application which finds devices attached to an _xkeys_server_ using the same MQTT broker. Either or both _xkeys-server_ and broker may exist remotely or on the local machine.
<p align="center" width="100%" ><img width="40%" src="assets/devicefinder-gtk.png" ></p>

## Building

### Dependencies

Some Linux distros already have most of dependent packages and build tools installed by default. On Debian, Ubuntu, RaspberryPi OS and similar systems the following command could be run to ensure the necessary dependencies are installed:
```
    sudo apt install build-essential libmosquitto-dev libjson-glib-dev libgtk-3-dev
```

### Compiling

To download the files to compile, clone the files from this repo with:
```
    git clone https://gitlab.com/chris.willing/devicefinder-gtk.git
```
There are two mechanisms available to build the project:
- use the project's built in Makefile. In this case enter the devicefinder-gtk directory that was created using the `git clone` command above and run the command: `make`, which should create the _devicefinder_ application.
- use the CMake build system. This may require installation of further development packages: 
    ```
        sudo apt install cmake libxml2-utils
    ```
    Now enter the devicefinder-gtk directory that was created using the `git clone` command above and run the following commands:

    ```
    mkdir build
    cd build
    cmake .. -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_BUILD_TYPE=Release
    make
    ```

## Installation
The _devicefinder_ app may be run immediately from the directory in which it was built (_devicefinder-gtk_ or _devicefinder-gtk/build_) using the command: `./devicefinder`.

However a system installation will enable _devicefinder_ to be run from anywhere with just the _`devicefinder`_ command (NB. no leading ./), as well as being available in the desktop menu system. To perform a system installation, enter the directory in which _devicefinder_ was built (_devicefinder-gtk_ or _devicefinder-gtk/build_) and run:
```
    sudo make install
```

## Usage
When run for the first time, _devicefinder_ will attempt to connect to the MQTT broker on the local machine. To connect to a remote broker, _devicefinder_ must be started from a terminal, specifying the broker's internet address and MQTT port number e.g.
```
    devicefinder test.mosquitto.org 1883
```
The remote broker specified in this way becomes the default for future invocations of _devicefinder_ and need not be specified again. To return to the local broker, run:
```
    devicefinder localhost 1883
```

Bear in mind that any broker connected to must be the same broker to which the target _xkeys-server_ is connected.

When connected, any devices attached to the _xkeys-server_ are displayed as a list in the uppermost section of the _devicefinder_ window. The list may be refreshed by pressing the _Find Devices_ button. Any device may be selected by clicking on it with the mouse pointer. In the image above, the _XK-12 Jog-Shuttle_ is selected.

Each device is listed with a unique identifer as well as the device name - important when two devices of the same type are connected e.g. two _XK-24_ devices connected here. The unique identifer is a concatenation of the device's Product Identifier (PID) and the Unit Identifier (UID).

At least one device is always selected and relevant information about it is displayed in the middle section of the _devicefinder_ window. This information may be refreshed by pressing the _Device Info_ button.

The lowermost section of the _devicefinder_ window contains a realtime stream of events originating from any devices connected to the _xkeys-server_ (whether or not the device is selected). In the image above, button presses ("down" and "up" events) from an _XK-3 Switch Interface_ can be seen, followed by "jog" events, as well as "down" and "up" events from the selected _XK-12 Jog-Shuttle_ device. This section may be cleared at any time by pressing the _Clear Data_ button.

In the status bar at the bottom of the _devicefinder_ window, a green light indicates that a heartbeat has recently been detected from the _xkeys-server_. It will turn red if the connection to the _xkeys-server_ is interrupted (heartbeat not received). This feature requires the latest version of the _xkeys-server_ to be running. [Please update!](https://gitlab.com/chris.willing/xkeys-server#upgrading)

## Acknowlegdements

Many thanks to [P.I. Engineering](https://xkeys.com/) for the donation of several X-keys devices for development and testing.
