TARGET=devicefinder
DESTDIR ?= /

CC=gcc
DEBUG=-g
OPT=-O0
WARN=-Wall
PTHREAD=-pthread

#CCFLAGS=$(DEBUG) $(OPT) $(WARN) $(PTHREAD) `pkg-config --cflags json-c` -pipe -std=gnu99
CCFLAGS=$(DEBUG) $(OPT) $(WARN) $(PTHREAD) `pkg-config --cflags json-glib-1.0` -pipe -rdynamic

GTKLIB=`pkg-config --cflags --libs gtk+-3.0`

LD=gcc
LDFLAGS=$(PTHREAD) $(GTKLIB) -lmosquitto `pkg-config --libs json-glib-1.0` -export-dynamic

PKGCONFIG=$(shell which pkg-config)
GLIB_COMPILE_RESOURCES = $(shell $(PKGCONFIG) --variable=glib_compile_resources gio-2.0)
RESOURCES = $(shell $(GLIB_COMPILE_RESOURCES) --sourcedir=. --generate-dependencies devicefinder-gtk.gresource.xml)

OBJS=    main.o devicefinder-gtk.o devicefinder-gtk-resources.o

$(TARGET): $(OBJS) devicefinder-gtk.css
	$(LD) -o $(TARGET) $(OBJS) $(LDFLAGS)
    
main.o: main.c devicefinder-gtk.h
	$(CC) -c $(CCFLAGS) main.c $(GTKLIB) -o main.o

devicefinder-gtk.o: devicefinder-gtk.c devicefinder-gtk.h
	$(CC) -c $(CCFLAGS) $(GTKLIB) devicefinder-gtk.c -o devicefinder-gtk.o

devicefinder-gtk-resources.o: devicefinder-gtk.gresource.xml $(RESOURCES) devicefinder-gtk-resources.c
	$(CC) -c $(CCFLAGS) devicefinder-gtk-resources.c $(GTKLIB) -o devicefinder-gtk-resources.o

devicefinder-gtk-resources.c: devicefinder-gtk.gresource.xml $(RESOURCES)
	$(GLIB_COMPILE_RESOURCES) devicefinder-gtk.gresource.xml --target=$@ --sourcedir=. --generate-source


install: $(TARGET)
	echo $(DESTDIR)
	install -d $(DESTDIR)/usr/bin
	install -d $(DESTDIR)/usr/share/applications
	install -d $(DESTDIR)/usr/share/pixmaps
	install -m 0755 $(TARGET) $(DESTDIR)/usr/bin
	install -m 0644 $(TARGET).desktop $(DESTDIR)/usr/share/applications/$(TARGET).desktop
	install -m 0644 assets/pielogox200.png $(DESTDIR)/usr/share/pixmaps/$(TARGET).png

clean:
	rm -f *.o $(TARGET) devicefinder-gtk.gresource devicefinder-gtk-resources.c


